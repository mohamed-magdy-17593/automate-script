/**
 * steper function
 * pipe
 * promises pipe not afunctional pipe
 * pipe(
 *    tap(data => ...),
 *    collect(data => ...),
 *    wait(2000),
 *    retry(num, data => ..., data => ...)
 * )(defaultData)
 */

export const pipe = (...operators) => (defaultData = {}) =>
  operators.reduce(
    (acc, currentOperatior) => acc.then(currentOperatior),
    Promise.resolve(defaultData),
  )

export const withPromise = operatorFn => (...args) => data =>
  new Promise((resolve, reject) =>
    operatorFn({
      args,
      data,
      resolve,
      reject,
    }),
  )

export const tap = withPromise(({args: [fn], data, resolve}) => {
  fn(data)
  resolve(data)
})

export const collect = withPromise(({args: [fn], data, resolve}) => {
  resolve({...data, ...fn(data)})
})

export const wait = withPromise(({args: [num], data, resolve}) => {
  setTimeout(() => resolve(data), num)
})

export const retry = withPromise(
  ({args: [numObj, retryFn, callbackFn], data, resolve}) => {
    let {timer, count} = {
      timer: typeof numObj === 'number' ? numObj : 100,
      count: Infinity,
      ...numObj,
    }
    ;(function self() {
      setTimeout(() => {
        if (retryFn(data) || count === 0) {
          resolve(callbackFn(data))
        } else {
          count -= 1
          self()
        }
      }, timer)
    })()
  },
)
