export const compose = (...funcs) => arg =>
  [...funcs].reverse().reduce((acc, fn) => fn(acc), arg)

export const identity = x => x

// utils
export const trim = str => (typeof str === 'string' ? str.trim() : str)

export const classSelectorFromArray = classArray =>
  classArray.reduce((acc, str) => `${acc}.${str}`, '')

// dom helpers
export const getElementDOM = classSelector =>
  classSelector && document.querySelector(classSelector)

export const createVirtualDom = content => {
  const el = document.createElement('div')
  el.appendChild(
    typeof content === 'string' ? document.createTextNode(content) : content,
  )
  el.getAttribute = _ => (typeof content === 'string' ? content : '')
  return el
}

export const getHtmlDOM = node => node.innerHTML

export const getTextDOM = node => node.innerText

export const clickOnDOM = node => node.click() || node

export const getAttr = attrName => node => node.getAttribute(attrName)

/**
 * =========
 * STAR*CODE
 * =========
 * get class query to query select with it
 * @param {object} tag
 * @param {string} name
 */
export const getTagClassQuery = tag => tagName => {
  const classNames = classSelectorFromArray(tag.classes)
  if (tag.name === tagName) {
    return classNames
  } else {
    const result =
      tag.children &&
      tag.children
        .map(tag => getTagClassQuery(tag)(tagName))
        .filter(identity)[0]
    return result && `${classNames} ${result}`
  }
}

export const PLACEHOLDER_NODE = createVirtualDom('{{ NotFound }}')

export const getElement = tag =>
  compose(
    getElementDOM,
    getTagClassQuery(tag),
  )

export const withElement = fn => tag => tagName => {
  const el = getElement(tag)(tagName)
  if (el) {
    return fn(el)
  } else {
    return fn(PLACEHOLDER_NODE)
  }
}

// dom helper functions

export const getText = withElement(
  compose(
    trim,
    getTextDOM,
  ),
)

export const getHtml = withElement(
  compose(
    trim,
    getHtmlDOM,
  ),
)

export const clickOn = withElement(clickOnDOM)
