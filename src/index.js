import {pipe, tap, wait, collect} from './lib/pipe'
import {getText, clickOn, clickOnDOM} from './lib/dom'
import {PROFILE} from './dom-data/PROFILE'
import {CONTACT_INFO} from './dom-data/CONTACT_INFO'
import * as Keysim from 'keysim'
/**
 * script test
 */

pipe(
  wait(5000),
  collect(() => ({name: getText(PROFILE)('full-name')})),
  collect(() => ({title: getText(PROFILE)('title')})),
  tap(() => clickOn(PROFILE)('contact-info')),
  wait(2000),
  collect(() => ({profileLink: getText(CONTACT_INFO)('profile-link')})),
  collect(() => ({profileEmail: getText(CONTACT_INFO)('profile-email')})),
  collect(() => ({profileContact: getText(CONTACT_INFO)('profile-contact')})),
  wait(1000),
  tap(() => clickOnDOM(document.querySelector('.artdeco-dismiss'))),
  wait(1000),
  tap(() => clickOn(PROFILE)('message')),
  wait(1000),
  tap(() => {
    console.log('type message')
    let input = document.querySelector(
      '.msg-form__contenteditable.t-14.t-black--light.t-normal.flex-grow-1 p',
    )
    let keyboard = Keysim.Keyboard.US_ENGLISH
    try {
      keyboard.dispatchEventsForInput('hello', window)
    } catch (error) {}
    try {
      keyboard.dispatchEventsForInput('hello', document)
    } catch (error) {}
    try {
      keyboard.dispatchEventsForInput('hello', input)
    } catch (error) {}
  }),
  tap(data => console.log('gg', data)),
)()
