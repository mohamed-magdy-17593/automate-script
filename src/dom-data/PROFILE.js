export const PROFILE = {
  name: 'profile-section',
  classes: [
    'pv-profile-section',
    'pv-top-card-section',
    'artdeco-container-card',
    'ember-view',
  ],
  children: [
    {
      name: 'profile-left',
      classes: ['pv-top-card-v2-section__info', 'mr5'],
      children: [
        {
          name: 'full-name',
          classes: ['pv-top-card-section__name', 'inline'],
        },
        {
          name: 'title',
          classes: ['pv-top-card-section__headline', 'mt1'],
        },
        {
          name: 'actions',
          classes: ['pv-top-card-v2-section__actions'],
          children: [
            {
              name: 'message',
              classes: [
                'pv-s-profile-actions',
                'pv-s-profile-actions--message',
              ],
            },
          ],
        },
      ],
    },
    {
      name: 'profile-right',
      classes: ['pv-top-card-v2-section__links'],
      children: [
        {
          name: 'contact-info',
          classes: [
            'pv-top-card-v2-section__entity-name',
            'pv-top-card-v2-section__contact-info',
            'ml2',
          ],
        },
      ],
    },
  ],
}

export default PROFILE
