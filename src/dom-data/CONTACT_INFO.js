export const CONTACT_INFO = {
  name: 'contact-info',
  classes: [
    'pv-profile-section',
    'pv-contact-info',
    'artdeco-container-card',
    'pv-contact-info--for-top-card-v2',
  ],
  children: [
    {
      name: 'contact-info-section',
      classes: ['pv-profile-section__section-info', 'section-info'],
      children: [
        {
          name: 'profile-link',
          classes: ['pv-contact-info__contact-type', 'ci-vanity-url'],
        },
        {
          name: 'profile-email',
          classes: ['pv-contact-info__contact-type', 'ci-email'],
        },
        {
          name: 'profile-contact',
          classes: ['pv-contact-info__contact-type', 'ci-connected'],
        },
      ],
    },
  ],
}

export default CONTACT_INFO
